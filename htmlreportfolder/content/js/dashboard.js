/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8235294117647058, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Open Login Page"], "isController": true}, {"data": [1.0, 500, 1500, "1 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "Submit Risk with attachment"], "isController": true}, {"data": [1.0, 500, 1500, "1 Open Risk Management Menu "], "isController": false}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [1.0, 500, 1500, "Logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "2 Login"], "isController": false}, {"data": [1.0, 500, 1500, "Logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "1 Open Login Page"], "isController": false}, {"data": [0.0, 500, 1500, "1 Login"], "isController": false}, {"data": [0.0, 500, 1500, "Submit Risk With Attachment"], "isController": true}, {"data": [1.0, 500, 1500, "1 Submit new Risk"], "isController": false}, {"data": [1.0, 500, 1500, "Logout"], "isController": false}, {"data": [1.0, 500, 1500, "2 Login-1"], "isController": false}, {"data": [1.0, 500, 1500, "2 Login-0"], "isController": false}, {"data": [1.0, 500, 1500, "Open Risk Management Menu"], "isController": true}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 110, 0, 0.0, 717.1090909090912, 2, 7978, 150.60000000000014, 7583.249999999999, 7973.27, 5.753739930955121, 15.027865591327545, 5.746690782247097], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["Open Login Page", 10, 0, 0.0, 32.7, 21, 89, 83.50000000000003, 89.0, 89.0, 0.8274720728175423, 1.1309054354571781, 0.2949485415804716], "isController": true}, {"data": ["1 Logout", 10, 0, 0.0, 23.7, 18, 39, 37.900000000000006, 39.0, 39.0, 0.835421888053467, 1.1128080618212197, 0.32870261591478694], "isController": false}, {"data": ["Submit Risk with attachment", 10, 0, 0.0, 113.10000000000001, 85, 153, 150.60000000000002, 153.0, 153.0, 0.8269930532583526, 5.616203507484287, 5.113304509179622], "isController": true}, {"data": ["1 Open Risk Management Menu ", 10, 0, 0.0, 41.3, 29, 50, 50.0, 50.0, 50.0, 0.8302200083022001, 5.57090597758406, 0.34943830427563305], "isController": false}, {"data": ["Login", 10, 0, 0.0, 7522.0, 6798, 8022, 8017.7, 8022.0, 8022.0, 0.5312649418264889, 2.8283632391223503, 0.666415837671997], "isController": true}, {"data": ["Logout-1", 10, 0, 0.0, 20.7, 18, 23, 23.0, 23.0, 23.0, 0.8345155637152633, 1.1172240100559125, 0.3245157201869315], "isController": false}, {"data": ["2 Login", 10, 0, 0.0, 44.19999999999999, 29, 60, 59.5, 60.0, 60.0, 0.8291186468783682, 2.8319583782439266, 0.6453198843379487], "isController": false}, {"data": ["Logout-0", 10, 0, 0.0, 35.0, 30, 43, 42.6, 43.0, 43.0, 0.8333333333333334, 0.4638671875, 0.3426106770833333], "isController": false}, {"data": ["1 Open Login Page", 10, 0, 0.0, 32.7, 21, 89, 83.50000000000003, 89.0, 89.0, 0.8337502084375521, 1.1394857585042522, 0.2971863535934634], "isController": false}, {"data": ["1 Login", 10, 0, 0.0, 7477.8, 6754, 7978, 7973.7, 7978.0, 7978.0, 0.5321696556862328, 1.0154878000106433, 0.25335225307327974], "isController": false}, {"data": ["Submit Risk With Attachment", 10, 0, 0.0, 7789.400000000001, 7131, 8316, 8309.4, 8316.0, 8316.0, 0.5208062080099994, 12.19688468374043, 4.899850593719077], "isController": true}, {"data": ["1 Submit new Risk", 10, 0, 0.0, 113.10000000000001, 85, 153, 150.60000000000002, 153.0, 153.0, 0.8270614506657844, 5.616668002026301, 5.113727410884129], "isController": false}, {"data": ["Logout", 20, 0, 0.0, 68.45000000000002, 49, 106, 82.9, 104.84999999999998, 106.0, 1.661405549094534, 4.2555670065625515, 1.655970286800133], "isController": false}, {"data": ["2 Login-1", 10, 0, 0.0, 39.800000000000004, 26, 57, 56.400000000000006, 57.0, 57.0, 0.8295313148071339, 2.354605194939859, 0.32322558067192037], "isController": false}, {"data": ["2 Login-0", 10, 0, 0.0, 3.3, 2, 5, 4.9, 5.0, 5.0, 0.8311170212765957, 0.4796778902094415, 0.32303181100398937], "isController": false}, {"data": ["Open Risk Management Menu", 10, 0, 0.0, 41.3, 29, 50, 50.0, 50.0, 50.0, 0.8302889405513119, 5.571368523746264, 0.3494673177515775], "isController": true}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 110, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
